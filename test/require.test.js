'use strict'

const MODULE_EMPTY = "'use strict'";
const MODULE_SET_GLOBAL = "'use strict'\n" +
    "window.global_value = 'foo';";
const MODULE_INC_GLOBAL = "'use strict'\n" +
    "window.global_value = window.global_value + 1;";
const MODULE_FOO = "'use strict'\n" +
    "function foo() { return 'foo'; }\n" +
    "module.exports = foo;";
const MODULE_USE_FOO = "'use strict'\n" +
    "var foo = require('./foo');\n" +
    "window.global_value = foo();";
const MODULE_USE_RELATIVE_FOO = "'use strict'\n" +
    "var foo = require('./../foo');\n" +
    "window.global_value = foo() + '1';";
const MODULE_USE_EVAL_FOO = "'use strict'\n" +
    "var foo = require('./f' + 'o' + \"o\");\n" +
    "window.global_value = foo() + '2';";
const MODULE_FAKE_REQUIRE = "'use strict'\n" +
    "function bar() { AP.require('foo', function(test) { }); }";

var modules = {};

function XMLHttpRequest() {
    this.onreadystatechange = null;
    this.readyState = 4;
    this.status = 404;
    this.statusText = 'Not found';
    this.responseText = '';

    this.url = null;
};

XMLHttpRequest.prototype.open = function(method, url, async) {
    let pos = global.window.location.href.indexOf('/', global.window.location.href.indexOf('://') + 3);
    const hostname = pos === -1 ? global.window.location.href : global.window.location.href.substring(0, pos);
    if (url.substring(0, hostname.length) !== hostname)
        return;

    pos = url.indexOf('?', hostname.length);
    if (pos === -1)
        pos = url.length;

    const moduleName = url.substring(hostname.length, pos);
    if (!(moduleName in modules))
        return;

    this.status = 200;
    this.responseText = modules[moduleName];
}

XMLHttpRequest.prototype.send = function() {
    const self = this;
    setImmediate(() => self.onreadystatechange.apply(self));
}

global.window = { location: { href: '' } };
global.XMLHttpRequest = XMLHttpRequest;

const assert = require('assert');
require('../require');

function counter(count, done, success) {
    return function(err) {
        if (err) {
            done(err);
        } else if (count === 0) {
            done(new Error('Counter less that zero'));
        } else {
            count -= 1;
            if (count === 0) {
                success();
                done();
            }
        }
    }
}

suite('require tests', function() {
    test('require simple module', function(done) {
        global.window.location.href = 'http://localhost:8000/index.html';
        modules = { '/test.js': MODULE_EMPTY };
        global.window.require('test.js', done);
    });

    test('execute module with require', function(done) {
        const callback = counter(1, done, () => {
            assert.equal(global.window.global_value, 'foo');
        });

        global.window.location.href = 'http://localhost:8001/index.html';
        global.window.global_value = '';
        modules = { '/test.js': MODULE_SET_GLOBAL };
        
        global.window.require('test.js', callback);
    });

    test('execute module only once with require', function(done) {
        const callback = counter(2, done, () => {
            assert.equal(global.window.global_value, 1);
        });

        global.window.location.href = 'http://localhost:8002/index.html';
        global.window.global_value = 0;
        modules = { '/test.js': MODULE_INC_GLOBAL };
        
        global.window.require('test.js', callback);
        global.window.require('test.js', callback);
    });

    test('require module from another', function(done) {
        const callback = counter(1, done, () => {
            assert.equal(global.window.global_value, 'foo');
        });

        global.window.location.href = 'http://localhost:8003/index.html';
        global.window.global_value = '';
        modules = { '/foo.js': MODULE_FOO, '/test.js': MODULE_USE_FOO };
        
        global.window.require('test.js', callback);
    });

    test('require module relative', function(done) {
        const callback = counter(1, done, () => {
            assert.equal(global.window.global_value, 'foo1');
        });

        global.window.location.href = 'http://localhost:8004/index.html';
        global.window.global_value = '';
        modules = { '/foo.js': MODULE_FOO, '/scripts/test.js': MODULE_USE_RELATIVE_FOO };
        
        global.window.require('scripts/test.js', callback);
    });

    test('require with evaluate', function(done) {
        const callback = counter(1, done, () => {
            assert.equal(global.window.global_value, 'foo2');
        });

        global.window.location.href = 'http://localhost:8005/index.html';
        global.window.global_value = '';
        modules = { '/foo.js': MODULE_FOO, '/test.js': MODULE_USE_EVAL_FOO };
        
        global.window.require('test.js', callback);
    });

    test('skip fake require', function(done) {
        global.window.location.href = 'http://localhost:8006/index.html';
        global.window.global_value = '';
        modules = { '/test.js': MODULE_FAKE_REQUIRE };
        
        global.window.require('test.js', done);
    });
});
