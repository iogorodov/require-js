Browser `require()`
===================
Provide synchronous `require()` in browser. With asynchronous `XMLHttpRequest`. Do not use in production, use browserify to build production code.

CommonJS is cool. Browserify allows you use this cool thing in browser so you can share code for server side and client side. But running build after any change to see result is not cool. There are couple efforts to make `require()` for browser but all of them use synchronous `XMLHttpRequest`.

Asynchronous
------------
Yes it's asynchronous, so you won't see any messages about **synchronous `XMLHttpRequest` is depricated**. But almost all calls like `var foo = require('./foo')` resolve synchronously, so you modules acts like regular CommonJS modules.

Here is the trick: the first `require()` call is asynchronous. It loads content of root module, then find all `require` in it (with `RegExp` and `eval`) and loads contents of all required modules, and repeat it recursivly. After all contents are loaded it executes root module with `new Function` call, providing synchronous version of `require()`.

Usage
-----
Somewhere in html:
```
<script src="../require.js"></script>
<script type="text/javascript">
    require('./code');
</script>
```

code.js:
```
var foo = require('./foo');
var bar = require('./bar/index');
```