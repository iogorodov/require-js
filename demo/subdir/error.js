'use strict'

function error() {
    throw new Error('Don\'t call me');
}

module.exports = error;