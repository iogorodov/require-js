(function(window) {

function parseUrl(url) {
    var schemePos = url.indexOf('://');
    if (schemePos === -1) {
        schemePos = 0;
    } else {
        schemePos += 3;
    }

    var hostNamePos = url.indexOf('/', schemePos);
    if (hostNamePos === -1)
        hostNamePos = url.length;

    var namePos = url.lastIndexOf('/');
    if (namePos < hostNamePos)
        namePos = hostNamePos;

    return {
        hostname: url.substring(0, hostNamePos),
        path: url.substring(hostNamePos + 1, namePos + 1)
    };
}

function normalizePath(path) {
    var parts = path.split('/').filter(function(p) { return p.length > 0; });
    var result = [];
    for (var i = 0; i < parts.length; ++i) {
        var p = parts[i];
        if (p === '..') {
            if (result.length === 0)
                return null;
            result.pop();
        } else if (p !== '.') {
            result.push(p);
        }
    }

    return '/' + result.join('/');
}

function getUrl(origin, path) {
    var parsed = parseUrl(origin);
    var basePath = path.charAt(0) === '/' ? '' : parsed.path;
    var result = parsed.hostname + normalizePath(basePath + path);
    if (result.toLowerCase().substr(-3) === '.js')
        return result;

    return result + '.js';
}

function getScript(url, callback) {
    var req = new XMLHttpRequest();

    req.onreadystatechange = function() {
        if (this.readyState !== 4)
            return;
        if (this.status !== 200) {
            callback(new Error(this.statusText));
        } else {
            callback(null, this.responseText);
        }
    }

    req.open('GET', url + '?time=' + Date.now(), true);
    req.send();
}

function collectRequire(content) {
    var regex = /require\(([^\)]+)\)/g;
    var m, result = [];
    while ((m = regex.exec(content)) !== null) {
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        try {
            result.push(eval(m[1]));
        } catch (e) {
            // just skip
        }
    }

    return result;        
}

function getScripts(url, cache, callback) {
    var urls = [url];

    function next(index) {
        if (index >= urls.length) {
            callback();
            return;
        }

        var url = urls[index];
        if (!(url in cache)) {
            cache[url] = { url: url, content: null, exports: null, ready: [] };
            getScript(url, function(err, content) {
                if (err) {
                    callback(new Error('Cannot load module \'' + url + '\': ' + err.message));
                    return;
                }

                cache[url].content = content;
                collectRequire(content).forEach(function(r) { urls.push(getUrl(url, r)); });
                next(index + 1);
            });
        } else {
            next(index + 1);
        }
    }

    next(0);
}

function resolveItem(url, cache) {
    function require(path) {
        return resolveItem(getUrl(url, path), cache);
    }

    function onReady(item, err, exports) {
        item.ready.forEach(function(callback) { callback(err, exports); });
        item.ready = null;
    }

    if (!(url in cache)) {
        throw new Error('Cannot find module \'' + url + '\'');
    }

    var item = cache[url];
    if (item.exports !== null)
        return item.exports;

    var module = { id: item.url, uri: item.url, exports: {} };
    try {
        var func = new Function('require', 'module', 'exports', item.content + '\n//# sourceURL=' + item.url);
        func(require, module, module.exports);
        item.exports = module.exports;
        onReady(item, null, item.exports);
        return item.exports;
    } catch (e) {
        onReady(item, e);
        throw e;
    }
}

window.require = function(path, callback) {
    if (!window.require.cache)
        window.require.cache = {};

    var cache = window.require.cache;
    var url = getUrl(window.location.href, path);
    if (url in cache) {
        var item = cache[url];
        if (item.exports !== null) {
            if (callback)
                callback(null, item.exports);
        } else {
            if (callback) {
                item.ready.push(callback);
            } else {
                item.ready.push(function(err) {
                    if (err)
                        throw err;
                });
            }
        }
    } else {
        getScripts(url, cache, function(err) {
            if (err) {
                if (callback) {
                    callback(err);
                } else {
                    throw err
                }
            } else {
                if (callback) {
                    try {
                        var exports = resolveItem(url, cache);
                        callback(null, exports);
                    } catch (e) {
                        callback(e);
                    }
                } else {
                    resolveItem(url, cache);
                }
            }
        });
    }
};

}(window));
